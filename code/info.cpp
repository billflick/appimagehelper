//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "info.h"
//#include "checkforupdates.h"
#include "widget.h"
#define QT  "<a href=\"https://doc.qt.io/qt-5/opensourcelicense.html\">Qt</a>"
#define LICENSE_ADDRESS  "<a href=\"http://bin.ceicer.com/appimagehelper/LICENSE\">"
void Info::getSystem()
{
    QIcon icon(":/images/appimagehelper.png");
    this->setWindowIcon(icon);
    const QString license =
        DISPLAY_NAME + tr(" is free software, license ") + " <br>" LICENSE_ADDRESS " GPL version " + LICENCE_VERSION + ".</a>";
    const QString syfte = tr("appImageHelper is a program for creating, deleting, controlling and organizing shortcuts to AppImage.<br>");
    /*  */
    /* */
    // STOP EDIT
    QString v = "";
    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" +
         QSysInfo::currentCpuArchitecture() + ").\n";
    v += DISPLAY_NAME " " VERSION + tr(" was created ") + BUILD_DATE_TIME "\n" +
         tr("by a computer with") + " " + QSysInfo::buildAbi() + ".";
    // write GCC version
    QString temp;
#if defined _MSC_VER || __GNUC__ || defined __clang__ || defined __ICC ||      \
    defined __clang__
#if defined __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined __GNUC__ && !defined __clang__
#if defined __GNUC__ && !defined __MINGW32__
#define COMPILER "GCC"
#endif
#ifdef __MINGW32__
#define COMPILER "MinGW GCC"
#endif
    temp = (QString(tr(" Compiled by") + " %1 %2.%3.%4%5")
            .arg(COMPILER)
            .arg(__GNUC__)
            .arg(__GNUC_MINOR__)
            .arg(__GNUC_PATCHLEVEL__)
            .arg("."));
#endif
#ifdef __ICC
#define INTEL "Intel ICC"
    temp = (QString(tr(" Compiled by") + " %1 %2 Build date: %3 Update: %4%5")
            .arg(INTEL)
            .arg(__INTEL_COMPILER)
            .arg(__INTEL_COMPILER_BUILD_DATE)
            .arg(__INTEL_COMPILER_UPDATE)
            .arg("."));
#endif
    v += temp;
#if defined __clang__
    v += (QString(tr(" Compiled by") + " %1 %2.%3.%4%5")
          .arg("Clang")
          .arg(__clang_major__)
          .arg(__clang_minor__)
          .arg(__clang_patchlevel__)
          .arg("."));
#endif
#endif
    v += "\n";
#else
    v += tr("Unknown compiler.");
    v += "\n";
#endif
    v += "<br>";
    QString CURRENT_YEAR_S = CURRENT_YEAR;
    CURRENT_YEAR_S = CURRENT_YEAR_S.right(4);

    if(CURRENT_YEAR_S != START_YEAR) {
        const QString auther =
            "<h3 style=\"color:green;\">Copyright &copy; " START_YEAR " - " +
            CURRENT_YEAR_S +
            " " PROGRAMMER_NAME "</h3><br><a style=\"text-decoration:none;\" href='" +
            APPLICATION_HOMEPAGE + "'>" + tr("Home page") +
            "</a> | <a style=\"text-decoration:none;\" href='" SOURCEKODE "'>" +
            tr("Source code") +
            "</a> | <a style=\"text-decoration:none;\" href='" WIKI "'>" +
            tr("Wiki") +
            "</a><br><a style=\"text-decoration:none;\"  "
            "href=\"mailto:" PROGRAMMER_EMAIL "\">" PROGRAMMER_EMAIL "</a>  <br>" +
            tr("Phone: ") + PROGRAMMER_PHONE "<br><br>";
        QMessageBox::about(nullptr, tr("About ") + DISPLAY_NAME + " " + VERSION,
                           "<h1 style=\"color:green;\">" DISPLAY_NAME " " VERSION
                           "</h1>" +
                           auther + syfte + "<br>" +
                           tr("This program uses ") + QT + tr(" version ") + QT_VERSION_STR +
                           tr(" running on ") + v + "<br>" + license);
    } else {
        const QString auther =
            QLatin1String("<h3 style=\"color:green;\">Copyright &copy; " START_YEAR " " PROGRAMMER_NAME " </h3 > <br> < a style = \"text-decoration:none;\" href='") +
            APPLICATION_HOMEPAGE + "'>" + tr("Home page") +
            "</a> | <a style=\"text-decoration:none;\" href='" SOURCEKODE "'>" +
            tr("Source code") +
            "</a> | <a style=\"text-decoration:none;\" href='" WIKI "'>" +
            tr("Wiki") +
            "</a><br><a style=\"text-decoration:none;\"  "
            "href=\"mailto:" PROGRAMMER_EMAIL "\">" PROGRAMMER_EMAIL "</a>  <br>" +
            tr("Phone: ") + PROGRAMMER_PHONE "<br><br>";
        QMessageBox::about(nullptr, tr("About ") + DISPLAY_NAME + " " + VERSION,
                           "<h1 style=\"color:green;\">" DISPLAY_NAME " " VERSION
                           "</h1>" +
                           auther + syfte + "<br>" +
                           tr("This program uses ") + QT + tr(" version ") + QT_VERSION_STR +
                           tr(" running on ") + v + "<br>" + license);
    }
}
