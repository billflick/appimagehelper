//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          appImageHelper
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);
    ui->pbSave->setVisible(false);
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui->txtBrowser->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c);     // set color "c" for textedit base
    ui->txtBrowser->setPalette(p);          // change textedit palette
}

void Dialog::setDialog(QPoint p, QString path)
{
    this->move(p);
    filePath = path;
    this->setWindowTitle(path);
    connect(ui->pbExit, &QPushButton::clicked,  [this]() {
        close();
    });
    QFile file(path);

    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    } else {
        QMessageBox::critical(
            nullptr, path,
            tr("Failure!\nCould not open file"));
        return;
    }

    QTextStream in(&file);
    QStringList sl;

    while(!in.atEnd()) {
        sl <<  in.readLine();
    }

    foreach(QString s, sl) {
        ui->txtBrowser->append(s);
    }

    file.close();
    ui->txtBrowser->setReadOnly(true);
    /*   */
//    connect(ui->pbSave, &QPushButton::clicked,  [this]() {
//        QFile *file2 = new QFile(filePath);
//        QString data = ui->txtBrowser->toPlainText();
//        if(file2->open(QIODevice::WriteOnly | QIODevice::Text)) {
//            QTextStream stream(file2);
//            stream << data;
//            file2->close();
//            file2->setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
//                                  QFileDevice::WriteUser | QFileDevice::ReadOther |
//                                  QFileDevice::ExeOther);
//        } else {
//            QMessageBox::critical(
//                nullptr, filePath,
//                tr("Failure!\nCould not open file"));
//            return;
//        }
//    });
    /*   */
}
Dialog::~Dialog()
{
    emit sendEnable();
    delete ui;
}
