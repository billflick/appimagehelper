<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="49"/>
        <location filename="../checkforupdates.cpp" line="117"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="65"/>
        <location filename="../checkforupdates.cpp" line="72"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="73"/>
        <source> is the same version as the latest official version. </source>
        <translation> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="81"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="131"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="171"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="192"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="194"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
</context>
</TS>
