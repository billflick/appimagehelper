<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="49"/>
        <location filename="../checkforupdates.cpp" line="117"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="65"/>
        <location filename="../checkforupdates.cpp" line="72"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="73"/>
        <source> is the same version as the latest official version. </source>
        <translation> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="81"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="131"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="171"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="192"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="194"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">MainWindow</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="32"/>
        <location filename="../update.cpp" line="64"/>
        <location filename="../update.cpp" line="72"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="33"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="34"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="65"/>
        <source>An unexpected error occurred. Error message:
</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="72"/>
        <source> is updated.</source>
        <translation> är uppdaterad.</translation>
    </message>
    <message>
        <source>An unexpected error occurred
</source>
        <translation type="vanished">Ett oväntat fel inträffade
</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="46"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
<context>
    <name>Categories</name>
    <message>
        <location filename="../categories.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="20"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="32"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../categories.ui" line="39"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="39"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation>Program för att presentera, skapa eller bearbeta multimedia (ljud / video).</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="41"/>
        <source>An audio application.</source>
        <translation>En ljudapplikation.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="41"/>
        <source>A video application.</source>
        <translation>En videoapplikation.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>An application for development.</source>
        <translation>En applikation för utveckling.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="42"/>
        <source>Educational software.</source>
        <translation>Utbildningsprogramvara.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="43"/>
        <source>A game.</source>
        <translation>Ett spel.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="44"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation>Applikation för visning, skapande eller bearbetning av grafik.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="45"/>
        <source>Network application such as a web browser.</source>
        <translation>Nätverksapplikation som till exempel en webbläsare.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="46"/>
        <source>An office type application.</source>
        <translation>Ett kontorsprogram.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="46"/>
        <source>Scientific software.</source>
        <translation>Vetenskaplig programvara.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="47"/>
        <source>Settings applications.</source>
        <translation>Program för att göra inställningar.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="48"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation>Systemapplikation, &quot;Systemverktyg&quot; som t.ex. en loggvisare eller nätverksmonitor.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="50"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation>Litet verktygsprogram, &quot;Tillbehör&quot;.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="84"/>
        <source>You must select at least one category.</source>
        <translation>Du måste välja minst en kategori.</translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="99"/>
        <source>These settings apply to all *.desktop files in line </source>
        <translation>Dessa inställningar gäller alla *.desktop-filer på rad </translation>
    </message>
    <message>
        <location filename="../categories.cpp" line="99"/>
        <source>Categories for</source>
        <translation>Kategorier för</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="30"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="37"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Failure!
Could not open file</source>
        <translation>Fel!
Kunde inte öppna filen</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, license </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source>appImageHelper is a program for creating, deleting, controlling and organizing shortcuts to AppImage.&lt;br&gt;</source>
        <translation>appImageHelper är ett program för att skapa, ta bort, kontrollera och organisera genvägar till AppImage. &lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="37"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="38"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="51"/>
        <location filename="../info.cpp" line="60"/>
        <location filename="../info.cpp" line="69"/>
        <source> Compiled by</source>
        <translation> Kompilerad av</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilerare.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="91"/>
        <location filename="../info.cpp" line="108"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="93"/>
        <location filename="../info.cpp" line="110"/>
        <source>Source code</source>
        <translation>källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="112"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="98"/>
        <location filename="../info.cpp" line="115"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="99"/>
        <location filename="../info.cpp" line="116"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source> version </source>
        <translation> version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <location filename="../info.cpp" line="120"/>
        <source>This program uses </source>
        <translation>Det här programmet använder </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="104"/>
        <location filename="../info.cpp" line="121"/>
        <source> running on </source>
        <translation> som körs på </translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="24"/>
        <source>Decide the location of the shortcut(s).</source>
        <translation>Bestäm plats för genvägarna.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="31"/>
        <source>Symbolic link (ln -s) in any folder.</source>
        <translation>Symbolisk länk (ln -s) i valfri mapp.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="44"/>
        <source>Shortcut on the desktop (~/Desktop).</source>
        <translation>Genväg på skrivbordet (~/Desktop).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="68"/>
        <source>Use Standard *.desktop file (Recommended).</source>
        <translation>Använd Standard *.desktop-fil (Rekommenderas).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="87"/>
        <source>Use Symbolic link (ln -s).</source>
        <translation>Använd symbolisk länk (ln -s).</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="99"/>
        <source>Decide the category for your AppImage.</source>
        <translation>Bestäm kategorin för din AppImage.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="176"/>
        <source>Search for Update</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="186"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="193"/>
        <location filename="../widget.cpp" line="204"/>
        <source>Launch AppImage</source>
        <translation>Starta AppImage</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="224"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="252"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="283"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="311"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove missing shortcuts from the database. Red background color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta bort saknade genvägar från databasen. Röd bakgrundsfärg.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="314"/>
        <source>Remove missing</source>
        <translation>Ta bort saknade</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="359"/>
        <source>Check your shortcuts</source>
        <translation>Kolla dina genvägar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="390"/>
        <source>Create Shortcut</source>
        <translation>Skapa genväg</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="418"/>
        <source>Delete selected shortcuts</source>
        <translation>Radera valda genvägar</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="428"/>
        <source>Resize to Content</source>
        <translation>Anpassa storleken till innehållet</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="435"/>
        <source>No horizontal scrollbar</source>
        <translation>Ingen vågrät rullningslist</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="454"/>
        <source>View Path.</source>
        <translation>Visa sökväg.</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="473"/>
        <source>Do not check for a new version when the program starts.</source>
        <translation>Sök inte efter en ny version när programmet startar.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="32"/>
        <source>*.desktop file in: </source>
        <translation>*.desktop fil i: </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="53"/>
        <source>
(Recommended).</source>
        <translation>
(Rekommenderas).</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="64"/>
        <source>Delete all settings</source>
        <translation>Radera alla inställningar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="65"/>
        <source>Force update</source>
        <translation>Tvinga uppdatering</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="82"/>
        <location filename="../widget.cpp" line="109"/>
        <source>There is no configuration file.</source>
        <translation>Det finns ingen konfigurationsfil.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="88"/>
        <source>All configuration files will be moved to the Trash and the program will close.</source>
        <translation>Alla konfigurationsfiler flyttas till papperskorgen och programmet stängs.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="89"/>
        <source>All shortcuts created by </source>
        <translation>Alla genvägar skapade av </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="89"/>
        <source> will remain.
Do you want to continue?</source>
        <translation> kommer att finnas kvar.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="102"/>
        <location filename="../widget.cpp" line="108"/>
        <source>Failed to move the configuration file to the Trash.</source>
        <translation>Det gick inte att flytta konfigurationsfilen till papperskorgen.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="103"/>
        <source>Unexpected error.</source>
        <translation>Oväntat fel.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="150"/>
        <source>Copy Path</source>
        <translation>Kopiera sökvägen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="176"/>
        <source>Make the AppImage Executable</source>
        <translation>Gör AppImagen körbar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="198"/>
        <source>Could not make the AppImage executable.</source>
        <translation>Kunde inte göra AppImagen körbar.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="229"/>
        <source>Add Icon</source>
        <translation>Lägg till ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="232"/>
        <source>Add Symbolic Link</source>
        <translation>Lägg till symbolisk länk</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="235"/>
        <source>Add *.desktop file</source>
        <translation>Lägg till * .desktop-fil</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="243"/>
        <source>Unable to add a shortcut</source>
        <translation>Det går inte att lägga till en genväg</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="244"/>
        <source>There is already a shortcut here. You must remove it before you can create a new one.</source>
        <translation>Det finns redan en genväg här. Du måste ta bort den innan du kan skapa en ny.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="277"/>
        <source>Remove AppImage</source>
        <translation>Ta bort AppImage</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="280"/>
        <source>Remove Icon</source>
        <translation>Ta bort ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="283"/>
        <source>Remove Symbolic Link</source>
        <translation>Ta bort symbolisk länk</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="286"/>
        <source>Remove the *.desktop file</source>
        <translation>Ta bort * .desktop-filen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="305"/>
        <source>Edit Categories</source>
        <translation>Redigera kategorier</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="339"/>
        <source>View the *.desktop file</source>
        <translation>Visa *.desktop-filen</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="487"/>
        <source>Application for presenting, creating, or processing multimedia (audio/video).</source>
        <translation>Program för att presentera, skapa eller bearbeta multimedia (ljud / video).</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="489"/>
        <source>An audio application.</source>
        <translation>En ljudapplikation.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="489"/>
        <source>A video application.</source>
        <translation>En videoapplikation.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="490"/>
        <source>An application for development.</source>
        <translation>En applikation för utveckling.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="490"/>
        <source>Educational software.</source>
        <translation>Utbildningsprogramvara.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="491"/>
        <source>A game.</source>
        <translation>Ett spel.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="492"/>
        <source>Application for viewing, creating, or processing graphics.</source>
        <translation>Applikation för visning, skapande eller bearbetning av grafik.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="493"/>
        <source>Network application such as a web browser.</source>
        <translation>Nätverksapplikation som till exempel en webbläsare.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="494"/>
        <source>An office type application.</source>
        <translation>Ett kontorsprogram.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="494"/>
        <source>Scientific software.</source>
        <translation>Vetenskaplig programvara.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="495"/>
        <source>Settings applications.</source>
        <translation>Program för att göra inställningar.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="496"/>
        <source>System application, &quot;System Tools&quot; such as say a log viewer or network monitor.</source>
        <translation>Systemapplikation, &quot;Systemverktyg&quot; som t.ex. en loggvisare eller nätverksmonitor.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="498"/>
        <source>Small utility application, &quot;Accessories&quot;.</source>
        <translation>Litet verktygsprogram, &quot;Tillbehör&quot;.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="581"/>
        <source>must be restarted for the new language settings to take effect.</source>
        <translation>måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="799"/>
        <source>Icon</source>
        <translation>Ikon</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="819"/>
        <source>Not executable</source>
        <translation>Ej körbar</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="832"/>
        <source>Missing</source>
        <translation>Saknas</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="835"/>
        <source>Missing!</source>
        <translation>Saknas!</translation>
    </message>
    <message>
        <source>zsync cannot be found in path:
</source>
        <translation type="vanished">zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <source>is updated.</source>
        <translation type="vanished">är uppdaterad.</translation>
    </message>
    <message>
        <source>An unexpected error occurred
</source>
        <translation type="vanished">Ett oväntat fel inträffade
</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="30"/>
        <source>Select AppImage</source>
        <translation>Välj AppImage</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="31"/>
        <source>AppImage (*.AppImage)</source>
        <translation>AppImage (*.AppImage)</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="40"/>
        <source>AppImage does not exist.</source>
        <translation>AppImage existerar inte.</translation>
    </message>
    <message>
        <location filename="../appimage.cpp" line="56"/>
        <location filename="../makedesktopfile.cpp" line="47"/>
        <location filename="../rewritedesktopfile.cpp" line="36"/>
        <location filename="../rewritedesktopfile.cpp" line="71"/>
        <location filename="../rewritedesktopfile.cpp" line="95"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Misslyckande!
Det gick inte att skapa genvägen.
Kontrollera filbehörigheterna.</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="40"/>
        <source>Select icon file (Not required</source>
        <translation>Välj ikonfil (Inte obligatoriskt</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="41"/>
        <source>Icon (*.ico *.png *.jpg *.jepg *.svg)</source>
        <translation>Icon (*.ico *.png *.jpg *.jepg *.svg)</translation>
    </message>
    <message>
        <location filename="../addshortcut.cpp" line="121"/>
        <source>Select Category</source>
        <translation>Välj kategori</translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="46"/>
        <source>Select icon file (Not required) png, xpm and svg are standard formats</source>
        <translation>Välj ikonfil (inte obligatoriskt) png, xpm och svg är standardformat</translation>
    </message>
    <message>
        <location filename="../icon.cpp" line="47"/>
        <source>Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)</source>
        <translation>Icon (*.png *.xpm *.svg *.ico *.jpg *.jepg)</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="29"/>
        <location filename="../symlink.cpp" line="84"/>
        <source>Select folder for the symbolic link</source>
        <translation>Välj mapp för den symboliska länken</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="41"/>
        <location filename="../symlink.cpp" line="96"/>
        <location filename="../symlink.cpp" line="106"/>
        <source>Failure!</source>
        <translation>Misslyckande!</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="42"/>
        <location filename="../symlink.cpp" line="97"/>
        <location filename="../symlink.cpp" line="107"/>
        <source>You do not have the right to create a link in this folder.</source>
        <translation>Du har inte rätt att skapa en länk i den här mappen.</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="60"/>
        <location filename="../symlink.cpp" line="68"/>
        <location filename="../widget.cpp" line="790"/>
        <source>An unexpected error occurred while creating the symbolic link.</source>
        <translation>Ett oväntat fel uppstod när den symboliska länken skapades.</translation>
    </message>
    <message>
        <location filename="../symlink.cpp" line="121"/>
        <location filename="../symlink.cpp" line="129"/>
        <source>An unexpected error occurred while creating the symbolic link</source>
        <translation>Ett oväntat fel uppstod när den symboliska länken skulle skapas</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="28"/>
        <source>Created</source>
        <translation>Skapad</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="29"/>
        <source>Path to AppImage</source>
        <translation>Sökväg till AppImage</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="30"/>
        <source>Path to Icon</source>
        <translation>Sökväg till ikon</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="31"/>
        <source>Symbolic link</source>
        <translation>Symbolisk länk</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="32"/>
        <location filename="../tableheader.cpp" line="39"/>
        <source>*.desktop file in:
</source>
        <translation>*.desktop fil i:
</translation>
    </message>
    <message>
        <location filename="../tableheader.cpp" line="33"/>
        <source>Symbolic link in:
</source>
        <translation>Symbolisk länk i:
</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="47"/>
        <location filename="../editdesktop.cpp" line="120"/>
        <source>Failed to create .desktop file.</source>
        <translation>Det gick inte att skapa .desktop-filen.</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="74"/>
        <source>Failed to edit *.desktop file.</source>
        <translation>Det gick inte att redigera *.desktop-filen.</translation>
    </message>
    <message>
        <location filename="../editdesktop.cpp" line="145"/>
        <source>Failed to create *.desktop file.</source>
        <translation>Det gick inte att skapa *.desktop-filen.</translation>
    </message>
    <message>
        <location filename="../config.cpp" line="79"/>
        <location filename="../widget.cpp" line="394"/>
        <source>Select &quot;Update&quot; to update</source>
        <translation>Välj &quot;Uppdatera&quot; för att uppdatera</translation>
    </message>
</context>
</TS>
